﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tweet_Service.Context;
using Tweet_Service.Interfaces;
using Tweet_Service.Models;

namespace Tweet_Service.Service
{
    public class TweetService : ITweetService
    {
        private readonly TweetServiceContext _tweetContext;
        public TweetService(TweetServiceContext tweetContext)
        {          
            _tweetContext = tweetContext;
        }
        public Tweet CreateTweet(Tweet tweet)
        {
            tweet.Id = Guid.NewGuid();
            _tweetContext.Add(tweet);
            _tweetContext.SaveChangesAsync();

            /*var kafkaContext = new KafkaContext(_config);
            kafkaContext.SendRideToKafkaTopic(ride);*/

            return tweet;
        }

        public Tweet GetById(Guid id)
        {
            return _tweetContext.Tweet.Find(id);
        }

        public IEnumerable<Tweet> GetMostRecentTweets()
        {
            return _tweetContext.Tweet.OrderByDescending(e => e.CreatedDate);
        }

        public IEnumerable<Tweet> GetTweetsByTrend(string trend)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Tweet> GetUserTweets(Guid userId)
        {
            return _tweetContext.Tweet.Where(e => e.UserId == userId);
        }
    }
}
